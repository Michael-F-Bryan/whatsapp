#!/bin/sh

PROJECT=whatsapp-summary
SERVICE=server
FULL_IMAGE=gcr.io/$PROJECT/$SERVICE
GIT_HASH=$(git rev-parse HEAD)
TIMEOUT=25m
REGION=asia-northeast1

set -xe

gcloud builds submit --project $PROJECT --tag ${FULL_IMAGE}:latest --tag $FULL_IMAGE:$GIT_HASH --timeout $TIMEOUT
gcloud run deploy $SERVICE --image $FULL_IMAGE:$GIT_HASH --region $REGION --platform managed
