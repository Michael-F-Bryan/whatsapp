# WhatsApp

[![pipeline status](https://gitlab.com/Michael-F-Bryan/whatsapp/badges/master/pipeline.svg)](https://gitlab.com/Michael-F-Bryan/whatsapp/pipelines)

([API Docs][docs] / [Online Version][online])

A utility crate for parsing the text file you get when [exporting a WhatsApp
conversation][export].

This repository also contains a simple web server which can convert an export
file into a HTML page that can give a more human-friendly view of the export.

## Deployment

We use the `Dockerfile` in this repository to build a container which is then
deployed to [Google Cloud Run][cloud-run] so it'll effectively spin up the
container on demand, then when requests drop off the container gets blown
away. I've also [played with DNS][dns] so that
`whatsapp-summary.michaelfbryan.com` gets rerouted to the Google servers,
giving us nice things like TLS certificates and a human-readable domain name.

This all makes deployment quite simple.

First you ask Google to build the image using the [gcloud][gcloud]
command-line tool, giving it a couple tags so we can refer to the image
later.

```console
$ gcloud builds submit \
  --project whatsapp-summary \
  --tag gcr.io/whatsapp-summary/server:latest \
  --tag gcr.io/whatsapp-summary/server:214a9d7975f05bdbfd2870a171fe51136e8ae402 \
  --timeout 25m
```

Then you tell Cloud Run to deploy the image, triggering a graceful transfer
of traffic from the old image to the new one.

```console
$ gcloud run deploy server \
  --image gcr.io/whatsapp-summary/server:214a9d7975f05bdbfd2870a171fe51136e8ae402 \
  --region asia-northeast1 \
  --platform managed
```

We also make sure it uses the `managed` platform instead of Kubernetes,
because Kubernetes is overkill for our use case (it keeps a pool of servers
running constantly instead of spinning up containers on demand) and would
cost a boat load for something that may get one or two page views a week.

Finally, this has all been wrapped up in a script so I don't need to remember
the incantation every time we want to deploy a change.

<details>
<summary>Example output from the deploy script</summary>

```console
$ ./deploy.sh
+ gcloud builds submit --project whatsapp-summary --tag gcr.io/whatsapp-summary/server:latest --tag gcr.io/whatsapp-summary/server:214a9d7975f05bdbfd2870a171fe51136e8ae402 --timeout 25m
Creating temporary tarball archive of 8012 file(s) totalling 2.2 MiB before compression.
Some files were not included in the source upload.
...
Logs are available at [https://console.cloud.google.com/cloud-build/builds/b47c2da0-9423-44cd-8850-2744198b0972?project=150437742570].
------------------------------------------------------------------------------------- REMOTE BUILD OUTPUT -------------------------------------------------------------------------------------
starting build "b47c2da0-9423-44cd-8850-2744198b0972"

FETCHSOURCE
Fetching storage object: gs://whatsapp-summary_cloudbuild/source/1589128159.118411-e4ecc9dbf09241abb1455acb878ffa90.tgz#1589128167056387
Copying gs://whatsapp-summary_cloudbuild/source/1589128159.118411-e4ecc9dbf09241abb1455acb878ffa90.tgz#1589128167056387...
...
Operation completed over 1 objects/1.8 MiB.
BUILD
Already have image (with digest): gcr.io/cloud-builders/docker
Sending build context to Docker daemon  1.465MB
Step 1/18 : FROM rust:1.43.0-slim AS build
...
Step 18/18 : ENTRYPOINT [ "/usr/local/bin/whatsapp-export-server" ]
 ---> Running in a3f6c67ab1fb
Removing intermediate container a3f6c67ab1fb
 ---> a7ba626b3525
Successfully built a7ba626b3525
Successfully tagged gcr.io/whatsapp-summary/server:214a9d7975f05bdbfd2870a171fe51136e8ae402
PUSH
Pushing gcr.io/whatsapp-summary/server:214a9d7975f05bdbfd2870a171fe51136e8ae402
The push refers to repository [gcr.io/whatsapp-summary/server]
040e86829888: Preparing
...
ccdaeb0056e9: Pushed
214a9d7975f05bdbfd2870a171fe51136e8ae402: digest: sha256:9d621ccbb6bcffa3fef8d4a7ad09589599233dc871a4a4d4033eb270284feff9 size: 1988
DONE
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ID                                    CREATE_TIME                DURATION  SOURCE                                                                                          IMAGES                                                                   STATUS
b47c2da0-9423-44cd-8850-2744198b0972  2020-05-10T16:29:29+00:00  18M32S    gs://whatsapp-summary_cloudbuild/source/1589128159.118411-e4ecc9dbf09241abb1455acb878ffa90.tgz  gcr.io/whatsapp-summary/server:214a9d7975f05bdbfd2870a171fe51136e8ae402  SUCCESS
+ gcloud run deploy server --image gcr.io/whatsapp-summary/server:214a9d7975f05bdbfd2870a171fe51136e8ae402 --region asia-northeast1 --platform managed
Deploying container to Cloud Run service [server] in project [whatsapp-summary] region [asia-northeast1]
✓ Deploying... Done.
  ✓ Creating Revision...
  ✓ Routing traffic...
Done.
Service [server] revision [server-00004-tiq] has been deployed and is serving 100 percent of traffic at https://server-egnveehjsa-an.a.run.app
```
</details>

Based on Google's pricing, I'm guessing this setup will cost about 10
cents/month.

## License

This project is licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE.md) or
   http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT.md) or
   http://opensource.org/licenses/MIT)

at your option.

It is recommended to always use [cargo-crev][crev] to verify the
trustworthiness of each of your dependencies, including this one.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.

The intent of this crate is to be free of soundness bugs. The developers will
do their best to avoid them, and welcome help in analysing and fixing them.

[docs]: https://michael-f-bryan.gitlab.io/whatsapp
[crev]: https://github.com/crev-dev/cargo-crev
[export]: https://faq.whatsapp.com/en/android/23756533/
[registry]: https://gitlab.com/Michael-F-Bryan/whatsapp/container_registry
[online]: https://whatsapp-summary.michaelfbryan.com/
[cloud-run]: https://cloud.google.com/run
[dns]: https://cloud.google.com/run/docs/mapping-custom-domains
[gcloud]: https://cloud.google.com/sdk
