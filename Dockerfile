# Dockerfile for the helper web server

FROM rust:1.43.0-slim AS build

WORKDIR /app

# To make things build faster, compile the dependencies in a separate step
RUN cargo search libc > /dev/null
COPY Cargo.toml Cargo.lock /app/
COPY export-parser/Cargo.toml /app/export-parser/Cargo.toml
COPY server/Cargo.toml /app/server/Cargo.toml
RUN mkdir -p /app/server/src /app/export-parser/src && \
    touch /app/export-parser/src/lib.rs && \
    echo 'fn main() { unimplemented!() }' >> /app/server/src/main.rs && \
    cargo build --verbose --release --workspace && \
    rm -r export-parser server

# And build everything else
COPY . /app/
RUN cargo clean --package whatsapp-export-parser --package whatsapp-export-server --release && \
    cargo build --verbose --release && \
    cp /app/target/release/whatsapp-export-server /usr/local/bin/whatsapp-export-server


# Now we can copy the compiled artifacts across to build the final image
FROM ubuntu:20.04
LABEL maintainer="michaelfbryan@gmail.com"

WORKDIR /app
COPY server/static /app/static
COPY server/templates /app/templates
COPY --from=build /usr/local/bin/whatsapp-export-server /usr/local/bin/

ENV RUST_LOG=info \
    PORT=80 \
    HOST=0.0.0.0 \
    WHATSAPP_STATIC_DIR=/app/static \
    WHATSAPP_TEMPLATES=/app/templates/**/*
EXPOSE $PORT

ENTRYPOINT [ "/usr/local/bin/whatsapp-export-server" ]
