use anyhow::Error;
use mime::{Mime, Name};
use mime_guess::MimeGuess;
use std::{collections::HashMap, path::Path};
use tera::{Context, Error as TeraError, Tera, Value};

const DEFAULT_NAME: &str = "WhatsApp Conversation";

pub fn render(
    tera: &Tera,
    body: &[u8],
    filename: &str,
) -> Result<String, Error> {
    let title = Path::new(&filename)
        .file_stem()
        .and_then(|stem| stem.to_str())
        .unwrap_or(DEFAULT_NAME);

    let text = std::str::from_utf8(&body)?;
    let parsed = whatsapp_export_parser::parse(text);

    let mut ctx = Context::new();
    ctx.insert("messages", &parsed.messages);
    ctx.insert("original_filename", &filename);
    ctx.insert("title", title);

    tera.render("report.html", &ctx).map_err(Error::from)
}

pub fn load_templates(templates: &str) -> Result<Tera, TeraError> {
    let mut tera = Tera::new(templates)?;
    register_helpers(&mut tera);

    Ok(tera)
}

pub fn register_helpers(tera: &mut Tera) {
    tera.register_tester("audio", mimetype_is(mime::AUDIO));
    tera.register_tester("image", mimetype_is(mime::IMAGE));
    tera.register_tester("video", mimetype_is(mime::VIDEO));
    tera.register_filter("mimetype", get_mimetype);
}

fn with_mimetype<F, T>(value: Option<&Value>, f: F) -> Result<T, TeraError>
where
    F: Fn(Mime) -> T + Send + Sync + 'static,
    T: Default,
{
    let value = value.ok_or_else(|| TeraError::from("No value provided"))?;
    let filename = value
        .as_str()
        .ok_or_else(|| TeraError::from("The input should be a string"))?;

    let mimetype = match MimeGuess::from_path(filename).first() {
        Some(guess) => guess,
        None => return Ok(T::default()),
    };

    Ok(f(mimetype))
}

fn mimetype_is(mime_type: Name<'static>) -> impl tera::Test {
    move |v: Option<&Value>, _args: &[Value]| -> Result<bool, TeraError> {
        with_mimetype(v, move |mime| mime.type_() == mime_type)
    }
}

fn get_mimetype(
    value: &Value,
    _args: &HashMap<String, Value>,
) -> Result<Value, TeraError> {
    with_mimetype(Some(value), |m| Value::from(m.essence_str()))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn render_a_dummy_report() {
        let template_pattern =
            concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*");
        let tera = load_templates(template_pattern).unwrap();
        let body = r#"
22/2/20, 3:57 pm - +60 12-345 6789: So sorry for disturbing
22/2/20, 3:58 pm - +60 12-345 6789: But pls let me know when you’re free
22/2/20, 3:58 pm - Michael Bryan: What is the problem
22/2/20, 3:58 pm - +60 12-345 6789: Cuz we having a big trouble here
22/2/20, 3:59 pm - +60 12-345 6789: IMG-20200222-WA0000.jpg (file attached)
22/2/20, 3:59 pm - +60 12-345 6789: IMG-20200222-WA0001.jpg (file attached)
"#;
        let filename = "my chat.txt";

        let got = render(&tera, body.as_bytes(), filename).unwrap();

        assert!(got.contains("Michael Bryan"));
        assert!(got.contains("+60 12-345 6789"));
        assert!(got.contains("my chat"));
        assert!(got.contains("What is the problem"));
    }
}
