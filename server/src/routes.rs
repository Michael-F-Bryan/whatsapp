use actix_files::Files;
use actix_multipart::Multipart;
use actix_web::{
    error::{ErrorBadRequest, InternalError},
    http::{
        header::{
            self, AcceptLanguage, ContentDisposition, DispositionParam,
            DispositionType, Header,
        },
        StatusCode,
    },
    middleware::Logger,
    web,
    web::Data,
    App, Error, HttpRequest, HttpResponse, HttpServer,
};
use bytes::{Bytes, BytesMut};
use futures::TryStreamExt;
use itertools::Itertools;
use std::{
    path::{Path, PathBuf},
    sync::Arc,
};
use tera::Tera;

pub async fn listen_and_serve(
    bind: &str,
    static_dir: PathBuf,
    templates: &str,
) -> Result<(), Box<dyn std::error::Error>> {
    let tera = Arc::new(crate::report::load_templates(templates)?);

    HttpServer::new(move || {
        App::new()
            .data(Arc::clone(&tera))
            .route("/", web::post().to(render_chat_summary))
            .service(
                Files::new("/", &static_dir)
                    .disable_content_disposition()
                    .index_file("index.html"),
            )
            .wrap(Logger::default())
    })
    .bind(bind)?
    .run()
    .await?;

    Ok(())
}

pub async fn render_chat_summary(
    multipart: Multipart,
    req: HttpRequest,
    tera: Data<Arc<Tera>>,
) -> Result<HttpResponse, Error> {
    let (body, filename) = read_form_field("file", multipart).await?;
    let locale = AcceptLanguage::parse(&req)
        .ok()
        .unwrap_or_else(|| AcceptLanguage(Vec::new()));

    log::info!(
        "Received {} bytes from {:?} (locale: \"{:?}\")",
        body.len(),
        req.connection_info(),
        locale.0.iter().format(", "),
    );

    let rendered =
        crate::report::render(&tera, &body, &filename).map_err(|e| {
            InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
        })?;

    let rendered_filename = Path::new(&filename)
        .with_extension("html")
        .display()
        .to_string();

    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .content_length(rendered.len() as u64)
        .header(
            header::CONTENT_DISPOSITION,
            ContentDisposition {
                disposition: DispositionType::Attachment,
                parameters: vec![DispositionParam::Filename(rendered_filename)],
            },
        )
        .body(rendered))
}

async fn read_form_field(
    name: &str,
    mut multipart: Multipart,
) -> Result<(Bytes, String), Error> {
    while let Some(field) = multipart.try_next().await? {
        let disposition = field.content_disposition();
        log::debug!(
            "Checking a {} field, content disposition: {:?}",
            field.content_type(),
            disposition
        );

        if let Some(disposition) = disposition {
            if let Some((field_name, filename)) =
                field_name_and_filename(&disposition)
            {
                if field_name == name {
                    let payload = field
                        .try_fold(BytesMut::new(), |mut buffer, chunk| {
                            buffer.extend_from_slice(&chunk);
                            futures::future::ok(buffer)
                        })
                        .await?;
                    return Ok((payload.freeze(), filename.to_string()));
                }
            }
        }
    }

    Err(ErrorBadRequest(format!("No \"{}\" field", name)))
}

fn field_name_and_filename(
    disposition: &ContentDisposition,
) -> Option<(&str, &str)> {
    let name = disposition.get_name()?;
    let filename = disposition.get_filename()?;

    Some((name, filename))
}
