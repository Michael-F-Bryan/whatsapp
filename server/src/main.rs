pub mod report;
pub mod routes;

use std::path::PathBuf;
use structopt::StructOpt;

#[actix_rt::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let args = Args::from_args();

    log::info!("Starting application with {:?}", args);
    let bind = format!("{}:{}", args.host, args.port);

    routes::listen_and_serve(&bind, args.static_dir, &args.templates).await
}

#[derive(Debug, Clone, PartialEq, StructOpt)]
pub struct Args {
    #[structopt(
        short = "p",
        long = "port",
        default_value = "8000",
        env = "PORT",
        help = "The port to bind to"
    )]
    port: u16,
    #[structopt(
        short = "H",
        long = "host",
        default_value = "localhost",
        env = "HOST",
        help = "The interface to serve on"
    )]
    host: String,
    #[structopt(
        short = "s",
        long = "static",
        default_value = "static",
        parse(from_os_str),
        env = "WHATSAPP_STATIC_DIR",
        help = "The location of all static assets on disk"
    )]
    static_dir: PathBuf,
    #[structopt(
        short = "t",
        long = "templates",
        default_value = "templates/**/*",
        env = "WHATSAPP_TEMPLATES",
        help = "A glob for all templates"
    )]
    templates: String,
}
