#![no_main]
use libfuzzer_sys::fuzz_target;

fuzz_target!(|src: String| {
    whatsapp_export_parser::parse(&src);
});
