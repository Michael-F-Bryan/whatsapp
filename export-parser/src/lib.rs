//! A utility crate for parsing the text file you get when [exporting a WhatsApp
//! conversation][export].
//!
//! # Examples
//!
//! The main purpose of this crate is the [`parse()`] function for parsing
//! source text into [`Message`]s and [`ParseError`]s.
//!
//! ```rust
//! use whatsapp_export_parser::{parse, Body, DirectMessage, Message, Span};
//!
//! let src = r#"
//! 31/10/19, 16:16 - Michael-F-Bryan: This is a message!
//! 31/10/19, 14:13 - +60 12-345 6789: IMG-20191031-WA0005.jpg (file attached)
//! "#;
//!
//! let parsed = dbg!(parse(src));
//!
//! assert!(parsed.errors.is_empty(), "Everything should have parsed successfully");
//! assert_eq!(parsed.messages.len(), 2);
//!
//! assert_eq!(parsed.messages[0].meta.sender, "Michael-F-Bryan");
//! let expected_body = Body::from(DirectMessage {
//!     content: String::from("This is a message!"),
//!     span: Span::new(36, 54),
//! });
//! assert_eq!(parsed.messages[0].body, expected_body);
//! assert_eq!(parsed.messages[1].meta.sender, "+60 12-345 6789");
//! ```
//!
//! # Cargo Features
//!
//! To help reduce compile times some features have been hidden behind cargo
//! feature flags.
//!
//! - `serde-1` - implements serialization for exported types
//!
//! [export]: https://faq.whatsapp.com/en/android/23756533/

#![forbid(unsafe_code)]
#![deny(
    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    rust_2018_idioms
)]

#[cfg(test)]
#[macro_use]
extern crate pretty_assertions;

mod messages;
mod parser;

pub use messages::{Attachment, Body, DirectMessage, Message, Metadata, Span};
pub use parser::{parse, ParseError, Parsed};
